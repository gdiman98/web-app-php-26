<?php
 
    function  getBooleanValue( $var ) {
 
        if(function_exists('boolval')) {
 
            $booleanValue = boolval($var);
 
        } else {
 
            $booleanValue = "boolval function is not available in current environment , please check your PHP version";
 
        }
 
        return $booleanValue;
 
    }
 
    $booleanValue = getBooleanValue( 'Numetric');
 
    var_dump( $booleanValue);
 
?>