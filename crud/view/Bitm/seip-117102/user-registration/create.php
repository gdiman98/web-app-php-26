<?php
    session_start();
   
    if(isset($_SESSION['Message'])&& !empty($_SESSION['Message'])){
        echo $_SESSION['Message'];
        
        unset($_SESSION['Message']);
    }

?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <title>Fullscreen Responsive Register Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- CSS -->
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Oleo+Script:400,700'>
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    </head>

    <body>

        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="logo span4">
                        <h1><a href="">User Register <span class="red">.</span></a></h1>
                    </div>
                    <div class="links span8">
                        <a class="home" href="" rel="tooltip" data-placement="bottom" data-original-title="Home"></a>
                        <a class="blog" href="" rel="tooltip" data-placement="bottom" data-original-title="Blog"></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="register-container container">
            <div class="row">
                <div class="iphone span5">
                    <img src="assets/img/iphone.png" alt="">
                </div>
                <div class="register span6">
                    <form action="store.php" method="post">
                        <h2>REGISTER TO <span class="red"><strong>User</strong></span></h2>
                        <label for="firstname">First Name</label>
                        <input type="text" name="firstname" placeholder="enter your first name...">
                        <label for="lastname">Last Name</label>
                        <input type="text" name="lastname" placeholder="enter your first name...">
                        <label for="username">Username</label>
                        <input type="text"  name="username" placeholder="choose a username...">
                        <label for="email">Email</label>
                        <input type="text" name="email" placeholder="enter your email...">
                        <label for="password">Password</label>
                        <input type="password"  name="password" placeholder="choose a password...">
                        <input type="password" name="cpassword" placeholder="confirm password...">
                        <label for="birthday">Date of Birth</label>
                        <fieldset>
                          <label class="birthday">Month
                            <select class="select-style" name="BirthMonth">
                                <option  value="January">January</option>
                                <option value="February">February</option>
                                <option value="March" >March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December" >December</option>
                            </select>
                          </label>    
                          <label class="birthday">Day <select name='birthday'><?php for($i=1; $i<=31; $i++) echo "<option>".$i."</option>"; ?></select></label>
                          <label class="birthday">Year <select name='birthyear'><?php for($k=1965; $k<=2010; $k++)	echo "<option>".$k."</option>";	?></select></label>
                         </fieldset>
                        <label for="gender">Gender</label>
                        <input  type="radio" name="gender" value="Male">Male
                        <input  type="radio" name="gender" value="female">Female
                        <label for="mobile">Mobile Number</label>
                        <input type="text" name="mobile-number" placeholder="enter your mobile number...">
                        <button type="submit" name="submit">REGISTER</button>
                    </form>
                </div>
            </div>
        </div>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.8.2.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
       <!-- <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>-->

    </body>

</html>

