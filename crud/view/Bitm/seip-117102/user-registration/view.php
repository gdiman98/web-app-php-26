<?php

include_once '../../../../src/Bitm/seip-117102/user-registration/Registration.php';

$obj= new Registration();

$userdata= $obj->index();

echo '<br> <a href="create.php">Register to User</a>';
?>

<html lang="en">

    <head>

        <meta charset="utf-8">
        <title>Fullscreen Responsive Register Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- CSS -->
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Oleo+Script:400,700'>
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    </head>

    <body>
        <div class="container">
  <h2>User Details</h2>
         
  <table class="table table-striped">
    <thead>
      <tr>
        <th>SL</th>  
        <th>Firs tname</th>
        <th>Last name</th>
        <th>Username</th>
        <th>Email</th>
        <th>Password</th>
        <th colspan="3">Date of Birth</th>
        <th>Gender</th>
        <th>Mobile Number</th>
        <th colspan="3">Action</th>
      </tr>
    </thead> 
    <tbody>
        <?php
$serial=1;

foreach ($userdata as $singledata){ ?>
<tr>
    <td><?php  echo $serial++ ?></td>
    <td><?php  echo $singledata['firstname'] ?></td>
    <td><?php  echo $singledata['lastname'] ?></td>
    <td><?php  echo $singledata['username'] ?></td>
    <td><?php  echo $singledata['email'] ?></td>
    <td><?php  echo $singledata['password'] ?></td>
    <td><?php  echo $singledata['BirthMonth'] ?></td>
    <td><?php  echo $singledata['birthday'] ?></td>
    <td><?php  echo $singledata['birthyear'] ?></td>
    <td><?php  echo $singledata['gender'] ?></td>
    <td><?php  echo $singledata['mobile-number'] ?></td>
    <td><a href="single-view.php?id=<?php echo $singledata['id']; ?>">View</a></td>
    <td>Edit</td>
    <td>Delete</td>
</tr>
<?php }

?>
        
      
    </tbody>
  </table>
</div>
    </body>
</html>
