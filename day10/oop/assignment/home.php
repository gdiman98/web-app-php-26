<?php
require_once './classes/calculator.php';
$obj= new Calculator();
?>


<html>
    <head>
        <title>Loop Example</title>
    </head>
    <body>
        <form action="calculator_view.php" method="post">
            <table>
                <tr>
                    <td>First Number </td>
                    <td>
                        <input type="text" name="first_number" value="<?php echo $_POST['first_number'];?>">
                    </td>
                </tr>
                <tr>
                    <td>Ending Number </td>
                    <td>
                        <input type="text" name="second_number" value="<?php echo $_POST['second_number'];?>">
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" name="btn" value="+">
                        <input type="submit" name="btn" value="-">
                        <input type="submit" name="btn" value="*">
                        <input type="submit" name="btn" value="/">
                        <input type="submit" name="btn" value="%">
                        
                    </td>
                </tr>
                <?php
                        if(isset($_POST['btn']))
                        {
                            $first_number=$_POST['first_number'];
                            $second_number=$_POST['second_number'];
                            
                            if($_POST['btn']=='+')
                            {
                                $result= $obj->add($first_number,$second_number);
                            }
                            if($_POST['btn']=='-')
                            {
                                $result= $obj->sub($first_number,$second_number);
                            }
                            if($_POST['btn']=='*')
                            {
                                $result= $obj->mul($first_number,$second_number);
                            }
                            if($_POST['btn']=='/')
                            {
                                $result= $obj->div($first_number,$second_number);
                            }
                            if($_POST['btn']=='%')
                            {
                                $result= $obj->rim($first_number,$second_number);
                            }
                            
                        }
                        
                        
                        
                        ?>
                <tr>
                    <td><?php echo $result['lbl'];?></td>
                    <td>
                        <?php echo $result['value'];?>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>