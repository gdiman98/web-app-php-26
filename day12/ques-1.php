<?php
$color = array('white', 'green', 'red', 'blue', 'black');

echo "The memory of that scene for me is like a frame of film forever frozen at that
	
Moment: the <b>$color[2]</b> carpet, the <b>$color[1]</b> lawn, the <b>$color[0]</b> house, the leaden sky. The new
	
President and his first lady. - Richard M. Nixon"
;
